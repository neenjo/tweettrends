import jdk.nashorn.api.scripting.JSObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;


/**
 * Created by nithin on 12/5/15.
 */
public class writerAndReader {
    public static void screenNameWriter(JSONArray jsonArray)
    {
        try {
            JSONObject jsonObject=new JSONObject();
//            FileWriter file = new FileWriter("screenName.json");
            PrintWriter file = new PrintWriter(new BufferedWriter(new FileWriter("screenName.json", false)));

            jsonObject.put("ScreenName",jsonArray);
            file.write(jsonObject.toString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public static JSONArray screenNameReader() {
//        JSONParser parser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        try {
//            Object obj = parser.parse(new FileReader("screenName.json"));
//            jsonObject=(JSONObject) obj;
            BufferedReader br = new BufferedReader(new FileReader("screenName.json"));
            String sCurrentLine="",full="";
            while ((sCurrentLine = br.readLine()) != null) {
                full=full+sCurrentLine;
            }
            jsonObject=new JSONObject(full);
            jsonArray = (JSONArray) jsonObject.get("ScreenName");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
}
