import au.com.bytecode.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by nithin on 8/5/15.
 */
public class BingScreenName {
    String bingOut;
    CSVWriter csvWriter;
    String[] temp;
    String ScreenName;
    String statusurl;
    String bingOutUrl;
    String status;
    String tweetcount;
    ConfigurationTwitter configurationTwitter;
    String ScreenNameList;
    String Authorization;
    JSONObject jsonObject;
    JSONObject jsonObject1;
    JSONArray jsonArray;
    JSONArray jsonArray1;
    JSONArray jsonArray2 = new JSONArray();
    String twitterDate;
    String date2;
    Date d = new Date();
    Date date = new Date();
    Date today = new Date((d.getTime() - 1 * 24 * 3600 * 1000));


    public BingScreenName() throws IOException {
        try {
            YamlReader yamlReader = new YamlReader();
            configurationTwitter = new ConfigurationTwitter(yamlReader.a);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            bingOut = configurationTwitter.getBingOutputFile();
            System.out.println(bingOut);
            csvWriter = new CSVWriter(new FileWriter(bingOut, true), '\t');


        } catch (Exception e) {
            System.out.println("Exception in File Writer: " + e);
        }
        bingOutUrl = configurationTwitter.getBingOutUrl();
        Authorization = configurationTwitter.getAuthorization();
        tweetcount = configurationTwitter.getStatusCount();
        tweetcount = tweetcount.replace("d", "%20");
        statusurl = configurationTwitter.getStatusUrl();

        try {

            try {

                jsonArray2 = writerAndReader.screenNameReader();
                System.out.println(jsonArray2 + "d");
                for (int iterator = 0; iterator < jsonArray2.length(); iterator++) {
                    System.out.println(jsonArray2.getString(iterator));

                    ScreenName = jsonArray2.getString(iterator);
                    statusurl = statusurl + "screen_name=" + ScreenName + "&count=" + tweetcount;
                    try {
                        status = GetConnection.getConnection(statusurl, Authorization);

                        jsonArray1 = new JSONArray(status);
                        int iterator1, length1 = jsonArray1.length();
                        for (iterator1 = 0; iterator1 < length1; iterator1++) {
                            jsonObject1 = jsonArray1.getJSONObject(iterator1);
                            twitterDate = jsonObject1.getString("created_at");
                            date = Utility.getTwitterDate(twitterDate);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                            date2 = sdf.format(date).toString();
                            if (date2.contains(sdf.format(today).toString())) {
                                temp = new String[]{ScreenName, (jsonObject1.getString("text")).replace("\n", " ")};
                                csvWriter.writeNext(temp);
                                System.out.println("Bond" + temp[0] + temp[1]);
                            }

                        }
                    } catch (Exception e) {
                        System.out.println("Exception in BindScreenName :" + e);
                    }
                }
            } catch (Exception e) {
                System.out.println("Exception :" + e);
                e.printStackTrace();
            }


            ScreenNameList = GetConnection.getConnection(bingOutUrl, Authorization);
            jsonObject = new JSONObject(ScreenNameList);
            jsonArray = jsonObject.getJSONArray("url");

        } catch (Exception e) {
            throw e;
        }
        csvWriter.close();
    }

    public void displayScreenNameTweets() {

        try {
            csvWriter = new CSVWriter(new FileWriter(bingOut, true), '\t');
        } catch (IOException e) {
            e.printStackTrace();
        }


        int iterator, length = jsonArray.length();
        for (iterator = 0; iterator < 4; iterator++) {
            ScreenName = jsonArray.getString(iterator);


            jsonArray2.put(ScreenName);
            statusurl = statusurl + "screen_name=" + ScreenName + "&count=" + tweetcount;

            try {
                status = GetConnection.getConnection(statusurl, Authorization);

                jsonArray1 = new JSONArray(status);
                int iterator1, length1 = jsonArray1.length();
                for (iterator1 = 0; iterator1 < length1; iterator1++) {
                    jsonObject1 = jsonArray1.getJSONObject(iterator1);
                    twitterDate = jsonObject1.getString("created_at");
                    date = Utility.getTwitterDate(twitterDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    date2 = sdf.format(date).toString();
//                    if(date2.contains(sdf.format(today).toString())) {
                    temp = new String[]{ScreenName, (jsonObject1.getString("text")).replace("\n", " ")};
                    csvWriter.writeNext(temp);
                    //                  System.out.println("Bond" + sdf.format(date));
                    //                                    }

                }
            } catch (Exception e) {
                System.out.println("Exception :" + e);
                e.printStackTrace();
            }

        }
        System.out.println(jsonArray2);
        writerAndReader.screenNameWriter(jsonArray2);
        try {
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
