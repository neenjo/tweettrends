import au.com.bytecode.opencsv.CSVWriter;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nithin on 6/5/15.
 */


public class TwitterTrendsApplication extends Application<Configuration> {
    static JSONObject jsonObject1 = new JSONObject();

    static Date date = new Date();
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");



    public static void main(String[] str) throws Exception {
        ConfigurationTwitter configurationTwitter = null;
        try {
            YamlReader yamlReader = new YamlReader();
            configurationTwitter = new ConfigurationTwitter(yamlReader.a);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {

            BingScreenName bingScreenName = new BingScreenName();
            bingScreenName.displayScreenNameTweets();
            System.out.println("Data Feteched");
        }catch (Exception e)
        {
            System.out.println("Exception in Application:" + e);
            e.printStackTrace();
        }


        System.out.println(sdf.format(date));
        String[] temp ;
        String temp2;
        String trendUrl;
        String Authorization;
        String trendsString1;
        String searchUrl;
        String apiUrl;
        String searchCount;
        String outputFile =configurationTwitter.getTrendOutputFile();
        JSONArray jsonArray, jsonArray1, jsonArray2 = new JSONArray();
        JSONObject jsonObject;
//        System.out.println(outputFile);
        CSVWriter fileWriter = new CSVWriter(new FileWriter(outputFile,true), '\t');

        searchUrl = configurationTwitter.getSearchUrl();
        apiUrl = configurationTwitter.getApiUrl();
        trendUrl = configurationTwitter.getTrendUrl();
        Authorization = configurationTwitter.getAuthorization();
        searchCount=configurationTwitter.getSearchCount();
        searchCount = searchCount.replace("d", "%20");

        String trendsString = GetConnection.getConnection(trendUrl, Authorization);
        jsonArray = new JSONArray(trendsString);
        jsonObject = jsonArray.getJSONObject(0);
        jsonArray1 = jsonObject.getJSONArray("trends");
        int iterator, length = jsonArray1.length();
        jsonArray = jsonObject.getJSONArray("trends");
        for (iterator = 0; iterator < length; iterator++) {
            jsonArray2.put(jsonArray.getJSONObject(iterator).getString("name"));

            trendUrl = jsonArray.getJSONObject(iterator).getString("url");
 //           System.out.println(trendUrl);

            trendUrl = trendUrl.replace(searchUrl, apiUrl);
            trendUrl = trendUrl + "&src=tren&vertical=news&count="+searchCount;
//            System.out.println(trendUrl);
            trendsString1 = GetConnection.getConnection(trendUrl, Authorization);
            jsonArray1 = new JSONObject(trendsString1).getJSONArray("statuses");
            int iterator1, length1 = jsonArray1.length();
            for (iterator1 = 0; iterator1 < length1; iterator1++) {
                //                   System.out.println(jsonArray1.getJSONObject(iterator1).getString("text"));
                temp2 = jsonArray1.getJSONObject(iterator1).getString("text");
                temp2 = temp2.replace("\n", " ");
                temp = new String[]{jsonArray.getJSONObject(iterator).getString("name"), temp2};
                fileWriter.writeNext(temp);

            }

        }

        jsonObject1.put("Trending", jsonArray2);


        fileWriter.flush();
        fileWriter.close();
       new TwitterTrendsApplication().run(str);
    }


    @Override
    public void run(Configuration configuration, Environment environment) throws Exception {
        environment.jersey().register(new TwitterResource(jsonObject1));



    }
}
