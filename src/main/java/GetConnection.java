/**
 * Created by nithin on 6/5/15.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;;
import java.net.URL;
public class GetConnection {

    public static String getConnection(String urlString,String Authorization) throws IOException {
        String output="",output1;
        HttpURLConnection conn=null;
        try {
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", Authorization);
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Page Fetch Failure "
                        + conn.getResponseCode());
            }



            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));


            while ((output1 = br.readLine()) != null) {
                output = output + output1;
            }




        }catch(Exception e){
            throw e;
        }
        return output;
    }

}
