
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by nithin on 7/5/15.
 */

@Path("/")
public class TwitterResource {
    private JSONObject jsonObject;

    public TwitterResource(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Path("/twitter")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    private String tweet() {
        return jsonObject.toString();
    }

}
