import java.util.LinkedHashMap;

/**
 * Created by nithin on 7/5/15.0

 */
public class ConfigurationTwitter {
    private String Authorization;
    private String trendUrl;
    private String searchUrl;
    private String apiUrl;
    private String statusUrl;
    private String searchCount;
    private String statusCount;
    private String bingOutUrl;
    private String trendOutputFile;
    private String bingOutputFile;


    public ConfigurationTwitter( LinkedHashMap<String,String> a) {

        this.Authorization=a.get("Authorization");
        this.trendUrl=a.get("trendUrl");
        this.apiUrl=a.get("apiUrl");
        this.searchUrl=a.get("searchUrl");
        this.statusUrl=a.get("statusUrl");
        this.statusCount=(a.get("statusCount"));
        this.searchCount=a.get("searchCount");
        this.bingOutUrl=a.get("bingOutUrl");
        this.trendOutputFile=a.get("TrendOutputFile");
        this.bingOutputFile=a.get("BingOutputFile");


    }

    public String getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(String searchCount) {
        this.searchCount = searchCount;
    }

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }

    public String getTrendUrl() {
        return trendUrl;
    }

    public void setTrendUrl(String trendUrl) {
        this.trendUrl = trendUrl;
    }

    public String getSearchUrl() {
        return searchUrl;
    }

    public void setSearchUrl(String searchUrl) {
        this.searchUrl = searchUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getStatusUrl() {
        return statusUrl;
    }
    public String getBingOutUrl() {
        return bingOutUrl;
    }

    public void setBingOutUrl(String bingOutUrl) {
        this.bingOutUrl = bingOutUrl;
    }

    public String getTrendOutputFile() {
        return trendOutputFile;
    }

    public void setTrendOutputFile(String trendOutputFile) {
        this.trendOutputFile = trendOutputFile;
    }

    public String getBingOutputFile() {
        return bingOutputFile;
    }

    public void setBingOutputFile(String bingOutputFile) {
        this.bingOutputFile = bingOutputFile;
    }


    public void setStatusUrl(String statusUrl) {
        this.statusUrl = statusUrl;
    }

    public String getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(String statusCount) {
        this.statusCount = statusCount;
    }
}
